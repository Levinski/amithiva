import './app.scss';
import ReactDOM from 'react-dom';

import Index from './components';

import ReactRouter from 'flux-react-router';
import React, { Component } from 'react';

var root = document.getElementById('root');

ReactRouter.createRoute('/', function () {
	ReactDOM.render(
		<div>
			<Index />
		</div>
	, root);
});

ReactRouter.createRoute('*', '/');

ReactRouter.init();