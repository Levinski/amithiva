import React from 'react';
import ContactForm from './contactForm';
import ComponentDefault from './ComponentDefault';

export default class Portfolio extends React.Component {
    state = {
        portfolio: false,
        form: false,
        transition: 'portfolio-transition '
    }
    
    handlePortfolio = () => {
        this.setState({portfolio: !this.state.portfolio});
    }

    handleForm = () => {
        this.setState({form: !this.state.form});
    }

    componentDidMount() {
        if (this.state.transition.search('portfolio-transition-open') == -1) {
            setTimeout(function() {
                this.setState({
                    transition: this.state.transition += 'portfolio-transition-open'
                });
            }.bind(this), 0);
        }
    }

    render() {
        var div = '';
        var form = '';
        var styles = {
            zIndex: 3
        }

        if (this.state.form) {
            styles.zIndex = 5;
            form = <ContactForm type="client" close={this.props.close} handleForm={this.handleForm}/>
        }

        if (this.state.portfolio) {
            div = (
                <p>
                    Toda <b>REVOLUÇÃO</b> começa com uma <b>AÇÃO</b>.
                    <br/>
                    <br/>
                    <b>Grandes avanços</b> sempre vem de <b>grandes iniciativas</b>.
                    Estas, por sua vez, surgem a partir de problemas e/ou necessidades.
                    <br/>
                    <br/>
                    O que você gostaria de aprimorar na comunicação da sua marca?
                    <br />
                    <br />
                    <button className="margin-top-5 btn" onClick={this.handleForm}>Conte para a gente!</button>
                </p>
            )
        } else {
            div = (
                <div>
                <p>
                    Quando desenvolvemos um job, não nos contentamos com <b>“bom”</b> ou <b>“legal”</b>. No nosso dicionário, só existe o <b>“uau”</b>. 
                    <br />
                    <br />
                    Unicidade da metodologia, processos internos e forma de enxergar o negócio do cliente;
                    <br />
                    Amithividade, que transmite toda nossa inquietação e energia para fazer acontecer;
                    <br />
                    União da equipe, que atua de forma sinérgica e colaborativa na busca por resultados; 
                </p>
                <p className="fonte">
                    <b>Fonte: Dicionário Amithiva</b>
                </p>
                <button className="margin-top-5 btn" onClick={this.handlePortfolio}>Conheça nosso portfólio</button>
                </div>
            )
        }

        

        return(
            <div className={this.state.transition} style={styles}>
                <div className="page portfolio">
                    {form}
                    <div className="row">
                        <div className="col-10">

                            <div className="text">
                                
                                {div}
                            </div>
                        </div>
                        <div className="col-2">
                            <button type="button"  className="close" onClick={this.props.close}>
                                X
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
