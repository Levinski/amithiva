import React, { Component } from 'react';
import ComponentDefault from './ComponentDefault';
import { render } from 'react-dom';
import { ScrollPage, Section } from 'react-scrollpage';

import Home from './home';
import Services from './services';
import Contact from './contact';
import Portfolio from './portfolio';
import {ToastContainer, ToastStore} from 'react-toasts';

export default class Index extends Component {
    
    state = {
        portfolio: false,
        width: 0,
        height:0
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('mousewheel', this.handleScroll);
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions = () => {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    currentPage = () => {
        var sections = document.getElementsByTagName('section');
        if (typeof(sections[0]) !== 'undefined') {
            var transform = sections[0].style.transform;

            if (transform == "translateY(0px)") {
                return 1;
            }
            if (transform == "translateY(-"+ (window.innerHeight) +"px)") {
                return 2;
            }
            if (transform == "translateY(-"+ (window.innerHeight*2) +"px)") {
                return 3;
            }
        }
    }

    changeMenuStyle = (style, withBlog=false) => {
        var navMenu = document.getElementsByClassName('nav-menu');
        if (typeof(navMenu[0]) !== 'undefined') {
            navMenu[0].style.color = style.color;
        }

        if (withBlog) {
            var navBlog = document.getElementsByClassName('nav-blog');
            if (typeof(navBlog[0]) !== 'undefined') {
                if (typeof(withBlog) == 'object') {
                    if (typeof(withBlog.display) !== 'undefined') {
                        navBlog[0].parentNode.style.display = withBlog.display;
                    }
                } else {
                    navBlog[0].style.color = style.color;
                }
            }
        }
    }

    handleScroll = (e) => {
        let currentPage = this.currentPage();

        if (currentPage == 1) {
            this.changeMenuStyle({color: "#000"}, true);
        }
        if (currentPage == 2) {
            this.changeMenuStyle({color: "#fff"}, true);
        }
        if (currentPage == 3) {
            this.changeMenuStyle({color: "#fff"}, true);
        }
        this.verifyPortfolioColor();
    }

    goHome = () => {
        window.turnTo(1);
        this.handleScroll();
    }

    goServices = () => {
        window.turnTo(2);
        this.handleScroll();
    }

    goContact = () => {
        window.turnTo(3);
        this.handleScroll();
    }

    verifyPortfolioColor = () => {
        let currentPage = this.currentPage();

        if (currentPage !== 1) {
            if (this.state.portfolio) {
                this.changeMenuStyle({color: "#000"}, false);
            } else {
                this.changeMenuStyle({color: "#fff"}, false);
            }
        }
    }

    handlePortfolio = () => {
        this.setState({portfolio: !this.state.portfolio});
        setTimeout(function() {
            this.verifyPortfolioColor();
        }.bind(this), 0);
    }

    render() {
        const options = {
            curPage:1,
            totalPage: 3,
            isEnable:true,
            delay:3200,
        }
        var portfolio = '';
        var centerClass = 'in-middle ';
        var home = '';
        var html = '';
        var navMenuClass = 'nav-menu';
        
        if (this.state.portfolio) {
            // this.props.changeMenuClass(this.props.menu.class + ' inPortfolio');
            // this.state.navMenuClass += ' inPortfolio';
            navMenuClass += ' inPortfolio';
            home = (
                <div className="item4">
                    <img src="/Assets/home.png" onClick={this.handlePortfolio}/>
                </div>
            );
            portfolio = <Portfolio close={this.handlePortfolio}/>
        }

        var content = (
            <ScrollPage
                {...options}
            >
                <Section>
                    <Home
                        portfolio={portfolio}/>
                </Section>
                <Section>
                    <Services
                        changeMenuStyle={this.changeMenuStyle}
                        currentPage={this.currentPage}
                        portfolio={portfolio}/>
                </Section>
                <Section>
                    <Contact
                        portfolio={portfolio}/>
                </Section>
                <ToastContainer store={ToastStore}/>
            </ScrollPage>
        );

        if (this.state.width >= 768) {
            html = (
                <div className="row h100">
                    <div className="col-1 fixed h100 z-index1 left-0">
                        <div className="nav-blog">
                            <p className="verticalText">
                                BLOG
                            </p>
                        </div>
                    </div>
                    <div className="col-10">
                        {content}
                    </div>
                    <div className="col-1 fixed h100 right-0">
                        <div className={navMenuClass}>
                            <div className="item1" onClick={this.handlePortfolio}>
                                PORTFOLIO
                            </div>
                            <div className="item2" onClick={this.goServices}>
                                SERVIÇOS
                            </div>
                            <div className="item3" onClick={this.goContact}>
                                CONTATO
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            html = (
                <div className="row h100">
                    <div className="col-10">
                        {content}
                    </div>
                    <div className="col-1 fixed h100 right-0">
                        <div className={navMenuClass}>
                            <div className="item1" onClick={this.handlePortfolio}>
                                PORTFOLIO
                            </div>
                            <div className="item2" onClick={this.goServices}>
                                SERVIÇOS
                            </div>
                            <div className="item3" onClick={this.goContact}>
                                CONTATO
                            </div>
                            <div className="blog">
                                BLOG
                            </div>
                            {home}
                        </div>
                    </div>
                </div>
            );
        }

        return (
            <div>
                {html}
            </div>
        )
    }
}