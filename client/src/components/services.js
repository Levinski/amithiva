import React, {Component} from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Portfolio from './portfolio';
import ComponentDefault from './ComponentDefault';

export default class Services extends ComponentDefault {

	state = {
        portfolio: false,

        howWeDo: false,
        bigData: false,
        businessInteligence: false,
        designThinking: false,

        whatWeDo: false,
        campanhasIntegradas: false,
        branding: false,
        marketingDigital: false,

        transitionEnter: true
    }

    handlePortfolio = () => {
        this.setState({portfolio: !this.state.portfolio});
    }

    //O QUE FAZEMOS
    handleWhatWeDo = () => {
        this.setState(
            {
                whatWeDo: !this.state.whatWeDo,
            }
        );
    }
    handleCampanhasIntegradas = (e) => {
    	this.setState(
    		{
                whatWeDo: !this.state.campanhasIntegradas,
    			campanhasIntegradas: !this.state.campanhasIntegradas,
    			branding: false,
    			marketingDigital: false,
                transitionEnter: (e.target.className.search('item') !== -1)
    		}
    	);
        this.verifyBlogDisplay();
    }
    handleBranding = (e) => {
    	this.setState(
    		{
                whatWeDo: !this.state.branding,
    			branding: !this.state.branding,
    			campanhasIntegradas: false,
    			marketingDigital: false,
                transitionEnter: (e.target.className.search('item') !== -1)
    		}
    	);
        this.verifyBlogDisplay();
    }
    handleMarketingDigital = (e) => {
    	this.setState(
    		{
                whatWeDo: !this.state.marketingDigital,
    			marketingDigital: !this.state.marketingDigital,
    			campanhasIntegradas: false,
    			branding: false,
                transitionEnter:  (e.target.className.search('item') !== -1)
    		}
    	);
        this.verifyBlogDisplay();
    }

    // COMO FAZEMOS
    handleHowWeDo = () => {
        this.setState(
            {
                howWeDo: !this.state.howWeDo,
            }
        );
    }
    handleBigData = (e) => {
    	this.setState(
    		{
    			bigData: !this.state.bigData,
                howWeDo: !this.state.bigData,
    			businessInteligence: false,
    			designThinking: false,
                transitionEnter: (e.target.className.search('item') !== -1)
    		}
    	);
        this.verifyBlogDisplay();
    }
    handleBusinessInteligence = (e) => {
    	this.setState(
    		{
    			businessInteligence: !this.state.businessInteligence,
                howWeDo: !this.state.businessInteligence,
    			bigData: false,
    			designThinking: false,
                transitionEnter: (e.target.className.search('item') !== -1)
    		}
    	);
        this.verifyBlogDisplay();
    }
    handleDesignThinking = (e) => {
    	this.setState(
    		{
    			designThinking: !this.state.designThinking,
                howWeDo: !this.state.designThinking,
    			businessInteligence: false,
    			bigData: false,
                transitionEnter: (e.target.className.search('item') !== -1)
    		}
    	);
        this.verifyBlogDisplay();
    }

    verifyBlogDisplay = () => {
        setTimeout(function() {
            if (!this.state.howWeDo && !this.state.whatWeDo) {
                this.props.changeMenuStyle({}, {display:"block"});
            } else {
                this.props.changeMenuStyle({}, {display:"none"});
            }
        }.bind(this), 0);
    }

	render() {
		var portfolio = '';

        var howWeDo = '';
		var bigData = '';
		var businessInteligence = '';
		var designThinking = '';

        var whatWeDo = '';
		var campanhasIntegradas = '';
		var branding = '';
		var marketingDigital = '';

        //O QUE FAZEMOS
        if (this.state.whatWeDo) {
            if (this.state.width < 768) {
                this.props.changeMenuStyle({color: "#000"}, false);
            }
            whatWeDo = (
                <div className="servicesPage">
                    <button type="button"  className="close" onClick={this.handleWhatWeDo}>
                        X
                    </button>
                    <div className="text">
                        <h1>CAMPANHAS INTEGRADAS</h1>
                        <ul>
                            <li>Planejamento Estratégico de Campanha</li>
                            <li>Comunicação Visual</li>
                            <li>Comunicação 360º (On-line / Off-line)</li>
                            <li>Inbound Marketing</li>
                            <li>Estratégias de Mídia</li>
                            <li>Assessoria de Imprensa</li>
                            <li>Trade Marketing</li>
                        </ul>

                        <h1>BRANDING</h1>
                        <ul>
                            <li>Posicionamento de marca</li>
                            <li>Branding Book</li>
                            <li>Identidade visual (logo, tipografia, paleta de cores, modelos de aplicação, etc)</li>
                            <li>Catálogos</li>
                            <li>Anúncios Institucionais</li>
                            <li>Site Institucional</li>
                        </ul>

                        <h1>MARKETING DIGITAL</h1>
                        <ul>
                            <li>Estratégias Inbound alinhadas ao digital</li>
                            <li>Mídia Programática</li>
                            <li>SEO</li>
                            <li>Planejamento e desenvolvimento de conteúdo</li>
                            <li>Gerenciamento de canais sociais</li>
                            <li>Análise de Redes Sociais</li>
                            <li>Campanhas de Google Adwords </li>
                            <li>Desenvolvimento WEB</li>
                            <li>Aplicativos (IOS e Android) </li>
                        </ul>
                    </div>
                </div>
            );
        }
        if (this.state.marketingDigital) {
        	marketingDigital = (
        		<div className="servicesPage whatWeDO">
        			<button className="close" onClick={this.handleMarketingDigital}>X</button>
        			<div className="text">
        				<h1>MARKETING DIGITAL</h1>
        				<ul>
        					<li>Estratégias Inbound alinhadas ao digital</li>
        					<li>Mídia Programática</li>
        					<li>SEO</li>
        					<li>Planejamento e desenvolvimento de conteúdo</li>
        					<li>Gerenciamento de canais sociais</li>
        					<li>Análise de Redes Sociais</li>
        					<li>Campanhas de Google Adwords </li>
        					<li>Desenvolvimento WEB</li>
        					<li>Aplicativos (IOS e Android) </li>
        				</ul>
        			</div>
        			<div className="nav">
        				<div className="row w100">
        					<div className="col-lg-6 col-md-12">
		    					<div className="item" onClick={this.handleCampanhasIntegradas}>
		    						<p>
			    						CAMPANHAS INTEGRADAS
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
        					<div className="col-lg-6 col-md-12">
		    					<div className="item" onClick={this.handleBranding}>
		    						<p>
			    						BRANDING
			    						<span className="margin-left-50">
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
    					</div>
    				</div>
        		</div>
        	);
        }
        if (this.state.branding) {
        	branding = (
        		<div className="servicesPage whatWeDO">
        			<button className="close" onClick={this.handleBranding}>X</button>
        			<div className="text">
        				<h1>BRANDING</h1>
        				<ul>
        					<li>Posicionamento de marca</li>
        					<li>Branding Book</li>
        					<li>Identidade visual (logo, tipografia, paleta de cores, modelos de aplicação, etc)</li>
        					<li>Catálogos</li>
        					<li>Anúncios Institucionais</li>
        					<li>Site Institucional</li>
        				</ul>
        			</div>
        			<div className="nav">
        				<div className="row w100">
        					<div className="col-lg-4 col-md-12">
		    					<div className="item" onClick={this.handleMarketingDigital}>
		    						<p>
			    						MARKETING<br/>
                                        DIGITAL
			    						<span className="margin-left-50">
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
        					<div className="col-lg-8 col-md-12">
		    					<div className="item" onClick={this.handleCampanhasIntegradas}>
		    						<p>
			    						CAMPANHAS INTEGRADAS
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
    					</div>
    				</div>
        		</div>
        	);
        }
        if (this.state.campanhasIntegradas) {
        	campanhasIntegradas = (
        		<div className="servicesPage whatWeDO">
        			<button className="close" onClick={this.handleCampanhasIntegradas}>X</button>
        			<div className="text">
        				<h1>CAMPANHAS INTEGRADAS</h1>
        				<ul>
        					<li>Planejamento Estratégico de Campanha</li>
        					<li>Comunicação Visual</li>
        					<li>Comunicação 360º (On-line / Off-line)</li>
        					<li>Inbound Marketing</li>
        					<li>Estratégias de Mídia</li>
        					<li>Assessoria de Imprensa</li>
        					<li>Trade Marketing</li>
        				</ul>
        			</div>
        			<div className="nav">
        				<div className="row w100">
        					<div className="col-lg-4 col-md-12">
		    					<div className="item" onClick={this.handleBranding}>
		    						<p>
			    						BRANDING
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
	    					<div className="col-lg-8 col-md-12">
		    					<div className="item" onClick={this.handleMarketingDigital}>
		    						<p>
			    						MARKETING DIGITAL
			    						<span className="margin-left-50">
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
    					</div>
    				</div>
        		</div>
        	)
        }

        //COMO FAZEMOS
        if (this.state.howWeDo) {
            if (this.state.width < 768) {
                this.props.changeMenuStyle({color: "#000"}, false);
            }
            howWeDo= (
                <div className="servicesPage">
                    <button type="button"  className="close" onClick={this.handleHowWeDo}>
                        X
                    </button>
                    <div className="text">
                        <p>
                            Desde que surgimos no mercado de comunicação, 
                            empregamos técnicas fundamentais na construção de resultados:
                        </p>
                        <h1>BIGDATA</h1>
                        <p>
                            Processo que envolve o levantamento,
                            armazenamento, estudo e análise de dados
                            relacionados ao universo do cliente, em busca de
                            identificar oportunidades e traçar estratégias de acordo com estas;
                        </p>
                        <p>
                            Desde que surgimos no mercado de comunicação, 
                            empregamos técnicas fundamentais na construção de resultados:
                        </p>
                        <h1>BUSINESS INTELIGENCE</h1>
                        <p>
                            Uma vez apurado os dados, chega o momento de gerar
                            insights para a equipe de Planejamento, Criação e
                            Mídia chegar a esforços de comunicaçnao funcionais e
                            alinhados as reais necessidades da marca, assim como
                            acompanhar os resultados da campanha durante (e
                            após) seu período de veiculação e trabalhar sobre
                            essas informações;
                        </p>
                        <p>
                            Desde que surgimos no mercado de comunicação, 
                            empregamos técnicas fundamentais na construção de resultados:
                        </p>
                        <h1>DESIGN THINKING</h1>
                        <p>
                            Após os insights, a equipe de criação se reúne para
                            entrar em ação. O famoso <b>brainsorming</b> (reunião onde
                            nascem as ideias) é o primeiro passo de um processo
                            que envolve muitos rabiscos, esboços, prototipação e
                            (eureca!) o resultado final que apresente uma solução
                            para o "problema" identificado durante as pesquisas;
                        </p>
                    </div>
                </div>
            );
        }
        if (this.state.designThinking) {
        	designThinking = (
        		<div className="servicesPage">
        			<button className="close" onClick={this.handleDesignThinking}>X</button>
        			<div className="text">
        				<p>
        					Desde que surgimos no mercado de comunicação, 
        					empregamos técnicas fundamentais na construção de resultados:
        				</p>
        				<h1>DESIGN THINKING</h1>
        				<p>
        					Após os insights, a equipe de criação se reúne para
        					entrar em ação. O famoso <b>brainsorming</b> (reunião onde
        					nascem as ideias) é o primeiro passo de um processo
        					que envolve muitos rabiscos, esboços, prototipação e
        					(eureca!) o resultado final que apresente uma solução
        					para o "problema" identificado durante as pesquisas;
        				</p>
        			</div>
        			<div className="nav">
        				<div className="row w100">
        					<div className="col-lg-4 col-md-12">
		    					<div className="item" onClick={this.handleBigData}>
		    						<p>
			    						BIGDATA
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
	    					<div className="col-lg-8 col-md-12">
		    					<div className="item" onClick={this.handleBusinessInteligence}>
		    						<p>
			    						BUSINESS INTELIGENCE
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
    					</div>
    				</div>
        		</div>
        	)
        }
        if (this.state.businessInteligence) {
        	businessInteligence = (
        		<div className="servicesPage">
        			<button className="close" onClick={this.handleBusinessInteligence}>X</button>
        			<div className="text">
        				<p>
        					Desde que surgimos no mercado de comunicação, 
        					empregamos técnicas fundamentais na construção de resultados:
        				</p>
        				<h1>BUSINESS INTELIGENCE</h1>
        				<p>
        					Uma vez apurado os dados, chega o momento de gerar
        					insights para a equipe de Planejamento, Criação e
        					Mídia chegar a esforços de comunicaçnao funcionais e
        					alinhados as reais necessidades da marca, assim como
        					acompanhar os resultados da campanha durante (e
        					após) seu período de veiculação e trabalhar sobre
        					essas informações;
        				</p>
        			</div>
        			<div className="nav">
        				<div className="row w100">
	    					<div className="col-lg-6 col-md-12">
		    					<div className="item" onClick={this.handleDesignThinking}>
		    						<p>
			    						DESIGN THINKING
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
	    					<div className="col-lg-6 col-md-12">
		    					<div className="item" onClick={this.handleBigData}>
		    						<p>
			    						BIGDATA
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
    					</div>
    				</div>
        		</div>
        	)
        }
        if (this.state.bigData) {
        	bigData = (
        		<div className="servicesPage">
        			<button className="close" onClick={this.handleBigData}>X</button>
        			<div className="text">
        				<p>
        					Desde que surgimos no mercado de comunicação, 
        					empregamos técnicas fundamentais na construção de resultados:
        				</p>
        				<h1>BIGDATA</h1>
        				<p>
        					Processo que envolve o levantamento,
        					armazenamento, estudo e análise de dados
        					relacionados ao universo do cliente, em busca de
        					identificar oportunidades e traçar estratégias de acordo com estas;
        				</p>
        			</div>
        			<div className="nav">
        				<div className="row w100">
        					<div className="col-lg-6 col-md-12">
		    					<div className="item" onClick={this.handleBusinessInteligence}>
		    						<p>
			    						BUSINESS INTELIGENCE
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
	    					<div className="col-lg-6 col-md-12">
		    					<div className="item" onClick={this.handleDesignThinking}>
		    						<p>
			    						DESIGN THINKING
			    						<span>
			        						>
			        					</span>
		        					</p>
		    					</div>
	    					</div>
    					</div>
    				</div>
        		</div>
        	)
        }

        if ((!this.state.howWeDo && !this.state.whatWeDo) || (this.state.width >= 768)) {
            if (this.props.currentPage() == 2) {
                this.props.changeMenuStyle({color: "#fff"}, false);
            }
        }

        var html = '';
        if (this.state.width >= 768) {
            if (this.props.currentPage() == 2) {
                this.props.changeMenuStyle({color: "#fff"}, false);
            }
            html = (
                <div className="h100">
                    <video loop muted autoPlay poster="/Assets/bg.png" className="imgBG">
                        <source src="Assets/movie.mp4" type="video/mp4"/>
                    </video>
                    
                    <div className="h15"></div>
                    
                    <div className="h70 row">
                        <div className="col-1"></div>
                        <div className="col-10 content">
                            <div className="in-middle h100">
                                <div className="row h100">
                                    <ReactCSSTransitionGroup 
                                        transitionName="transition-right" 
                                        transitionEnterTimeout={1000}
                                        transitionLeave={false}
                                        transitionEnter={this.state.transitionEnter}
                                    >
                                        {bigData}
                                        {businessInteligence}
                                        {designThinking}

                                        {campanhasIntegradas}
                                        {branding}
                                        {marketingDigital}
                                        
                                    </ReactCSSTransitionGroup>
                                    <div className="col-md-12">
                                        <div className="item1">
                                            <p className="verticalText">
                                                Como fazemos
                                            </p>
                                            <div className="subItens">
                                                <div className="item" onClick={this.handleBigData}>
                                                    BIG DATA
                                                </div>
                                                <div className="item" onClick={this.handleBusinessInteligence}>
                                                    BUSINESS INTELIGENCE
                                                </div>
                                                <div className="item" onClick={this.handleDesignThinking}>
                                                    DESIGN THINKING
                                                </div>
                                            </div>
                                        </div>
                                        <div className="item2">
                                            <p className="verticalText">
                                                O que fazemos
                                            </p>
                                            <div className="subItens">
                                                <div className="item" onClick={this.handleCampanhasIntegradas}>
                                                    CAMPANHAS INTEGRADAS
                                                </div>
                                                <div className="item" onClick={this.handleBranding}>
                                                    BRANDING
                                                </div>
                                                <div className="item margin-0" onClick={this.handleMarketingDigital}>
                                                    MARKETING DIGITAL
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {this.props.portfolio}
                                </div>
                            </div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="h15">
                        
                    </div>
                </div>
            );
        } else {
            if ((!this.state.howWeDo && !this.state.whatWeDo)) {
                if (this.props.currentPage() == 2) {
                    this.props.changeMenuStyle({color: "#fff"}, false);
                }
            }
            html = (
                <div className="h100">
                    <video loop muted autoPlay poster="/Assets/bg.png" className="imgBG">
                        <source src="Assets/movie.mp4" type="video/mp4"/>
                    </video>
                    
                    <div className="h100 row">
                        <div className={((this.state.width <= 364) ? "col-10" : "col-11") + " content"}>
                            <div className="in-middle h100">
                                <div className="row h100">
                                    <ReactCSSTransitionGroup 
                                        transitionName="transition-right" 
                                        transitionEnterTimeout={1000}
                                        transitionLeave={false}
                                        transitionEnter={this.state.transitionEnter}
                                    >
                                        {howWeDo}
                                        {whatWeDo}
                                    </ReactCSSTransitionGroup>
                                    <div className="col-md-12">
                                        <div className="item1">
                                            <p className="verticalText">
                                                Como fazemos
                                            </p>
                                            <div className="subItens">
                                                <div className="item" onClick={this.handleBigData}>
                                                    BIG DATA
                                                </div>
                                                <div className="item" onClick={this.handleBusinessInteligence}>
                                                    BUSINESS INTELIGENCE
                                                </div>
                                                <div className="item" onClick={this.handleDesignThinking}>
                                                    DESIGN THINKING
                                                </div>
                                            </div>
                                        </div>
                                        <div className="item2">
                                            <p className="verticalText">
                                                O que fazemos
                                            </p>
                                            <div className="subItens">
                                                <div className="item" onClick={this.handleCampanhasIntegradas}>
                                                    CAMPANHAS INTEGRADAS
                                                </div>
                                                <div className="item" onClick={this.handleBranding}>
                                                    BRANDING
                                                </div>
                                                <div className="item margin-0" onClick={this.handleMarketingDigital}>
                                                    MARKETING DIGITAL
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {this.props.portfolio}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

		return(
            <div className="container-fluid services">
                {html}
            </div>
        );
	}
}
