import React from 'react';
import MediaQuery from 'react-responsive';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Portfolio from './portfolio';
import ReactRouter from 'flux-react-router';
import ComponentDefault from './ComponentDefault';

export default class Home extends ComponentDefault {
    state = {
        portfolio: false,
        width:0,
        height:0
    }

    handlePortfolio = () => {
        this.setState({portfolio: !this.state.portfolio});
    }

    render() {
        var portfolio = '';
        var centerClass = 'in-middle ';
        var styles = {
            backgroundColor: '#e5e5e5'
        };

        if (this.props.portfolio !== '') {
            centerClass += 'home-transition';
            styles.backgroundColor = '#fff';
        }

        var html = '';

        if (this.state.width >= 768) {
            html = (
                <div className="container-fluid home">
                    <div className="h100">
                        <div className="h15"></div>
                        <div className="h70 row">
                            <div className="col-1"></div>
                            <div className="col-10 col-offset-1">
                                <div className=" content">
                                    <div className="in-middle h100">

                                        <div className={centerClass}>
                                            <img src="/Assets/amithiva.png"/>
                                            <h1>Inovação Criativa</h1>
                                        </div>
                                        <div className="h100">
                                            {this.props.portfolio}
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div className="col-1"></div>
                        </div>
                        <div className="h15"></div>
                    </div>
                </div>
            );
        }

        if (this.state.width < 768) {
            html = (
                <div className="container-fluid home important"  style={styles}>
                    <div className="h100">
                        <div className="h15"></div>
                        <div className="h70 row">
                            <div className={(this.state.width <= 364) ? "col-10" : "col-11"}>
                                <div className=" content">
                                    <div className="in-middle h100">

                                        <div className={centerClass}>
                                            <img src="/Assets/amithiva.png"/>
                                            <h1>Inovação Criativa</h1>
                                        </div>
                                        <div className="h100">
                                            {this.props.portfolio}
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="h15"></div>
                    </div>
                </div>
            );
        }

        return(
            html
        );
    }
}
