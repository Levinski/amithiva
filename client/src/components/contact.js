import React, {Component} from 'react';
import MediaQuery from 'react-responsive';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import Portfolio from './portfolio';
import ReactRouter from 'flux-react-router';
import ContactForm from './contactForm';
import ComponentDefault from './ComponentDefault';

export default class Contact extends ComponentDefault {
    constructor(props) {
        super(props);

    }
    state = {
        portfolio: false,
        form: false,
        formWorkWithUs: false,
        width:1000,
        height:0
    }
    componentWillMount () {
        this.resize();
    }
    componentDidMount () {
        this.resize();
        window.addEventListener("resize", this.resize);
    }
    componentWillUnmount () {
        window.removeEventListener("resize", this.resize);
    }

    updateWindowDimensions = () => {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    handlePortfolio = () => {
        this.setState({portfolio: !this.state.portfolio});
    }
    handleForm = () => {
        this.setState({form: !this.state.form});
    }
    handleFormWorkWithUs = () => {
        this.setState({formWorkWithUs: !this.state.formWorkWithUs});
    }

    iframe = (height) => {
    	var iframe = '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1329.1212966032072!2d-47.34429759033625!3d-22.74800655765603!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94c89bda365b800d%3A0x891f77073b069707!2sR.+M%C3%A9xico%2C+571+-+Vila+Frezzarin%2C+Americana+-+SP%2C+13465-780!5e0!3m2!1spt-BR!2sbr!4v1524595417058" width="600" height="'+ height +'" frameborder="0" style="border:0" allowfullscreen></iframe>'
    	return {
    		__html: iframe
    	}
    }

    getPxWithPercent = (totalPX, percent) => {
        var x = Math.floor((totalPX*percent)/100);
        return x;
    }

    resize = () => {
        this.updateWindowDimensions();
        var iframePercent = 52;
        if (this.state.width <= 992) {
            iframePercent = 26
        }
        if (this.state.width <= 768) {
            iframePercent = 35
        }
        if (this.state.width <= 388) {
            iframePercent = 30
        }
        
        this.setState({
            iframeHeight: this.getPxWithPercent(document.body.clientHeight, iframePercent)
        })
    }

	render() {
		var portfolio = '';
        var navMenuClass = 'nav-menu';
        var form = '';
        var formWorkWithUs = '';
        var html = '';

		if (this.state.portfolio) {
            portfolio = <Portfolio close={this.handlePortfolio}/>
            navMenuClass += ' inPortfolio';
        }

        if (this.state.form) {
            form = <ContactForm handleForm={this.handleForm} type="geral"/>
            navMenuClass += ' hidden';
        } 
        
        if (this.state.formWorkWithUs) {
            formWorkWithUs = <ContactForm handleForm={this.handleFormWorkWithUs} type="workWithUs"/>
            navMenuClass += ' hidden';
        }

        if (this.state.width >= 768) {
            html = (
                <div className="h100">
                    <div className="h15"></div>
                    <div className="h70 row">
                        <div className="col-1"></div>
                        <div className="col-10 important content">
                            {form}
                            {formWorkWithUs}
                            <div className="row h100">
                                <div className="col-md-12 h100">
                                    <div className="row">
                                        <div className="col-lg-6 col-sm-12 padding-right-50px">
                                            <h1>SOBRE</h1>
                                            <div className="text">
                                                <p>
                                                    Não é à toa que temos tanta sede de inovação, 
                                                    energia e vontade de fazer diferente: somos uma agência jovem, 
                                                    com apenas cinco anos de mercado. Apesar disso, 
                                                    experiência e know how em comunicação integrada 
                                                    já fazem parte da nossa bagagem. Toda vez que 
                                                    recebemos uma nova solicitação de trabalho, 
                                                    seja qual for o tamanho do projeto, entramos 
                                                    em amithividade e começamos a levantar dados, 
                                                    apurar, esboçar, prototipar, layoutar... 
                                                    Até chegar àquilo que acreditamos traduzir 
                                                    a real necessidade de nossos clientes. 
                                                    Técnicas como Design Thinking, Strategy Copy, 
                                                    Daily, não são apenas nomes em inglês para parecer cool. 
                                                    São preceitos que seguimos com o propósito de entregar 
                                                    trabalhos de grande impacto e efetividade. 
                                                </p>
                                            </div>
                                            
                                            <div className="row">
                                                <div className="col-12 d-none d-lg-block">
                                                    <p className="address">
                                                        +55 19 3407.1523 / 3645.3192 - Rua México, 571 
                                                        Villa Frezzarin - Americana/SP - Brasil
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 d-none d-lg-block">
                                            <div className="h100 maps" dangerouslySetInnerHTML={ this.iframe(this.state.iframeHeight) } />
                                        </div>
                                    </div>
                                    <div className="row bottom">
                                        <div className="col-6">
                                            <div className="row">
                                                <div className="col-6">
                                                    <button className="btn-small" onClick={this.handleForm}>FALE CONOSCO</button>
                                                </div>
                                                <div className="col-6">
                                                    <button className="btn-small" onClick={this.handleFormWorkWithUs}>TRABALHE CONOSCO</button>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="row icons">
                                                <div className="col-5"></div>
                                                <div className="col-6">
                                                    <img src="/Assets/ICONS-REDES.png" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12 d-none d-sm-block d-lg-none">
                                            <p className="address">
                                                +55 19 3407.1523 / 3645.3192 - Rua México, 571 
                                                Villa Frezzarin - Americana/SP - Brasil
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12 d-none d-sm-block d-lg-none">
                                            <div className="h100 maps" dangerouslySetInnerHTML={ this.iframe(this.state.iframeHeight) } />
                                        </div>
                                    </div>
                                </div>

                                {this.props.portfolio}
                                {(this.props.portfolio != '') ? 
                                    <div className="blur" onClick={this.handlePortfolio}></div> 
                                : null}

                            </div>
                        </div>
                        <div className="col-1"></div>
                    </div>
                    <div className="h15 relative">
                        <div className="row rodape">
                            <div className="col-md-11">
                                <p>Amithiva 2018 - Todos os direitos reservados</p>
                            </div>
                            <div className="col-md-1">
                                
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            html = (
                <div className="h100">
                    <div className="h15"></div>
                    <div className="h70 row">
                        <div className="col-10 content">
                            {form}
                            {formWorkWithUs}
                            <div className="row h100">
                                <div className="col-md-12 h100">
                                    <div className="row">
                                        <div className="col-lg-6 col-sm-12 padding-right-50px">
                                            <h1>SOBRE</h1>
                                            <div className="text">
                                                <p>
                                                    Não é à toa que temos tanta sede de inovação, 
                                                    energia e vontade de fazer diferente: somos uma agência jovem, 
                                                    com apenas cinco anos de mercado. Apesar disso, 
                                                    experiência e know how em comunicação integrada 
                                                    já fazem parte da nossa bagagem. Toda vez que 
                                                    recebemos uma nova solicitação de trabalho, 
                                                    seja qual for o tamanho do projeto, entramos 
                                                    em amithividade e começamos a levantar dados, 
                                                    apurar, esboçar, prototipar, layoutar... 
                                                    Até chegar àquilo que acreditamos traduzir 
                                                    a real necessidade de nossos clientes. 
                                                    Técnicas como Design Thinking, Strategy Copy, 
                                                    Daily, não são apenas nomes em inglês para parecer cool. 
                                                    São preceitos que seguimos com o propósito de entregar 
                                                    trabalhos de grande impacto e efetividade. 
                                                </p>
                                            </div>
                                            
                                            <div className="row">
                                                <div className="col-12 d-none d-lg-block">
                                                    <p className="address">
                                                        +55 19 3407.1523 / 3645.3192 - Rua México, 571 
                                                        Villa Frezzarin - Americana/SP - Brasil
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6 d-none d-lg-block">
                                            <div className="h100 maps" dangerouslySetInnerHTML={ this.iframe(this.state.iframeHeight) } />
                                        </div>
                                    </div>
                                    <div className="row bottom">
                                        <div className="col-5">
                                            <div className="row icons">
                                                <div className="col-12">
                                                    <img src="/Assets/ICONS-REDES.png" />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-6">
                                            <div className="row">
                                                <div className="col-6">
                                                    <button className="btn-small" onClick={this.handleForm}>FALE CONOSCO</button>
                                                </div>
                                                <div className="col-6">
                                                    <button className="btn-small" onClick={this.handleFormWorkWithUs}>TRABALHE CONOSCO</button>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    <div className="row">
                                        <div className="col-12 d-none d-block d-lg-none">
                                            <p className="address">
                                                +55 19 3407.1523 / 3645.3192 - Rua México, 571 
                                                Villa Frezzarin - Americana/SP - Brasil
                                            </p>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-12 d-none d-block d-lg-none">
                                            <div className="h100 maps" dangerouslySetInnerHTML={ this.iframe(this.state.iframeHeight) } />
                                        </div>
                                    </div>
                                </div>

                                {this.props.portfolio}
                                {(this.props.portfolio != '') ? 
                                    <div className="blur" onClick={this.handlePortfolio}></div> 
                                : null}

                            </div>
                        </div>
                        <div className="col-2"></div>
                    </div>
                    <div className="h15 relative">
                        <div className="row rodape">
                            <div className="col-md-11">
                                <p>Amithiva 2018 - Todos os direitos reservados</p>
                            </div>
                            <div className="col-md-1">
                                
                            </div>
                        </div>
                    </div>
                </div>
            );
        }

        return (
			<div className="container-fluid contact ">
                {html}
            </div>
		);
	}
}