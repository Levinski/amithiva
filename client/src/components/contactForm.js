import React, {Component} from 'react';
import {ToastStore} from 'react-toasts';
import ComponentDefault from './ComponentDefault';

export default class ContactForm extends ComponentDefault {

    constructor(props) {
        super(props);

    }

    state = {
        name: '',
        company: '',
        email: '',
        phone: '',
        description: '',
        selectedFile: {}
    }

    getBase64 = (file, cb) => {
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            cb(reader.result)
        };
        reader.onerror = function (error) {
            console.log('Error: ', error);
        };
    }

    getPxWithPercent = (totalPX, percent) => {
        var x = Math.floor((totalPX*percent)/100);
        return x;
    }

    sendEmail = (e) => {
        e.preventDefault();

        if (this.state.name == '') {
            return ToastStore.error("O nome é obrigatório", 4000);
        }

        if (this.state.description == '') {
            return ToastStore.error("A mensagem é obrigatória", 4000);
        }

        const dataToSend = {
            name: this.state.name,
            company: this.state.company,
            email: this.state.email,
            phone: this.state.phone,
            description: this.state.description,
            selectedFile: this.state.selectedFile
        };

        return fetch('/api/form/' + this.props.type, {
            method: "POST",
            body: JSON.stringify(dataToSend),
            headers: {
                "Content-Type": "application/json",
            }
        }).then(response => response.json())
            .then(response => {
                if (response.error) {
                    return ToastStore.error("Erro ao enviar mensagem, tente novamente mais tarde !", 4000);
                }
                ToastStore.success("Mensagem enviada com sucesso!", 4000);
                return this.props.handleForm();
            })
        .catch(err => {
            return ToastStore.error("Erro ao enviar mensagem, tente novamente mais tarde !", 4000);
        });
            
    }

    changeName = (e) => {
        this.setState({name: e.target.value});
    }

    changeCompany = (e) => {
        this.setState({company: e.target.value});
    }

    changeEmail = (e) => {
        this.setState({email: e.target.value});
    }

    changePhone = (e) => {
        this.setState({phone: e.target.value});
    }

    changeDescription = (e) => {
        this.setState({description: e.target.value});
    }

    handdleAnnex = (e) => {
        this.getBase64(e.target.files[0], (result) => {
             this.setState({selectedFile: result});
        });
    }

    render() {
        var textareaPlaceholder = '';
        var formClass = 'contact-form ';
        var fileUpload = '';

        if (this.props.type == 'geral') {
            textareaPlaceholder = "Mensagem";
            formClass += 'contact-form-geral';
        }

        if (this.props.type == 'client') {
            textareaPlaceholder = "Quais pontos do seu negócio você deseja melhorar/evoluir?"
        }

        if (this.props.type == 'workWithUs') {
            textareaPlaceholder = "Mensagem"
            formClass += 'contact-form-geral';
            fileUpload = (
                <div className="row">
                    <div className="col-12">
                        <label htmlFor="annex">
                            <img src="/Assets/annex.png"/>Anexar Documento
                        </label>
                        <input multiple id="annex" type="file" name="annex" onChange={this.handdleAnnex}/>
                    </div>
                </div>
            );
        }

        return(
            <div>
                <div className="blur" onClick={this.props.handleForm}>
                </div>
                <div className={formClass}>
                    <button type="button"  className="close" onClick={this.props.handleForm}>
                        X
                    </button>
                    <form className="h100">
                        <div className="row margin-bottom-1">
                            <div className="col-md-12">
                                <input
                                    type="text"
                                    name="name"
                                    placeholder="Nome"
                                    value={this.state.name}
                                    onChange={this.changeName}/>
                            </div>
                        </div>
                        <div className="row margin-bottom-1">
                            <div className="col-md-12">
                                <input
                                    type="text"
                                    name="company"
                                    placeholder="Empresa"
                                    value={this.state.company}
                                    onChange={this.changeCompany}/>
                            </div>
                        </div>
                        <div className="row margin-bottom-1">
                            <div className="col-md-12">
                                <input
                                    type="text"
                                    name="email"
                                    placeholder="E-mail"
                                    value={this.state.email}
                                    onChange={this.changeEmail}/>
                            </div>
                        </div>
                        <div className="row margin-bottom-1">
                            <div className="col-md-12">
                                <input
                                    type="text"
                                    name="phone"
                                    placeholder="Telefone(Opcional)"
                                    value={this.state.email}
                                    onChange={this.changePhone}/>
                            </div>
                        </div>
                        {fileUpload}
                        <div className="row h40">
                            <div className="col-md-12">
                                <textarea
                                    rows="4"
                                    cols="50"
                                    name="description"
                                    placeholder={textareaPlaceholder}
                                    value={this.state.description}
                                    onChange={this.changeDescription}>
                                
                                </textarea>
                            </div>
                        </div>
                        <div className="row text-center">
                            <div className="col-md-12">
                                <button className="btn" onClick={this.sendEmail}>Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}