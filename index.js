const express = require('express');
const bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
const config = require('./config');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

let transporter = nodemailer.createTransport({
    host: config.smtp.host,
    port: config.smtp.port,
    secure: config.smtp.secure, // true for 465, false for other ports
    auth: {
        user: config.smtp.auth.user,
        pass: config.smtp.auth.pass
    }
});

let mailOptions = {
    from: '"Site AMITHIVA" <'+ config.smtp.auth.user +'>', // sender address
    to: config.smtp.to, // list of receivers
};

app.get('/api/teste', (req, res) => {
    return res.json({teste:true});
})

app.post('/api/form/client', (req, res) => {

    const message = 'Nome: ' + req.body.name + '\n' +
        'Empresa: ' + req.body.company + '\n' +
        'Email: ' + req.body.email + '\n' +
        'Telefone: ' + req.body.phone + '\n' +
        'Mensagem: ' + req.body.description;

    nodemailer.createTestAccount((err, account) => {

        mailOptions.subject = 'Contato - Cliente';
        mailOptions.text = message;

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                return res.json({error: true});
            }
            return res.json({error: false});
        });
    });
});

app.post('/api/form/workWithUs', (req, res) => {

	const message = 'Nome: ' + req.body.name + '\n' +
		'Empresa: ' + req.body.company + '\n' +
        'Email: ' + req.body.email + '\n' +
        'Telefone: ' + req.body.phone + '\n' +
		'Mensagem: ' + req.body.description;

    nodemailer.createTestAccount((err, account) => {

        mailOptions.subject = 'Contato - Trabalhe conosco';
        mailOptions.text = message;
        mailOptions.attachments = [
            {
                path: req.body.selectedFile
            }
        ]

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
            	console.log(error);
            	return res.json({error: true});
            }
            return res.json({error: false});
        });
    });
});

app.post('/api/form/geral', (req, res) => {

    const message = 'Nome: ' + req.body.name + '\n' +
        'Empresa: ' + req.body.company + '\n' +
        'Email: ' + req.body.email + '\n' +
        'Telefone: ' + req.body.phone + '\n' +
        'Mensagem: ' + req.body.description;
    
    nodemailer.createTestAccount((err, account) => {
        mailOptions.subject = 'Contato - Geral';
        mailOptions.text = message;

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                console.log(error);
                return res.json({error: true});
            }
            return res.json({error: false});
        });
    });
});

const PORT = 3001;

app.listen(PORT, () => {
	console.log('server listening on port: ' + PORT);
});

